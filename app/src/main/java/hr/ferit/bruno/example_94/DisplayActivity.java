package hr.ferit.bruno.example_94;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DisplayActivity extends Activity {

    @BindView(R.id.tvDisplay) TextView tvDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        ButterKnife.bind(this);

        Intent startingIntent = this.getIntent();
        if(startingIntent.hasExtra(MainActivity.MSG_KEY)){
            String message = startingIntent.getStringExtra(MainActivity.MSG_KEY);
            this.tvDisplay.setText(message);
        }
    }
}
