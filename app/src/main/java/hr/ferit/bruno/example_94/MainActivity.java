package hr.ferit.bruno.example_94;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    public static final String MSG_KEY = "msg";
    @BindView(R.id.bSendNotification) Button bSendNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bSendNotification)
    public void onClick(){

        // Set-up of the required data:
        int notificationID = 1;
        String message = "Hello world!";
        String title = "My cool notification";
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.cupcake);
        Intent intent = new Intent(this, DisplayActivity.class);
        intent.putExtra(MSG_KEY, "This is a message from the notification.");
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT
        );

        // Creating the notification:
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setLargeIcon(bitmap)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setLights(Color.RED, 2000, 1000)
                .setVibrate(new long[]{1000,1000,1000,1000,1000})
                .setSound(soundUri);
        Notification notification = builder.build();

        // Actual notification:
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationID, notification);

        // This activity is no longer needed, so we can shut it down:
        this.finish();
    }
}
